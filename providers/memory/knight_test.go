package memory

import (
    "testing"

    "gitlab.com/PuKoren/go-assignment/domain"
)

func TestProvider_Read(t *testing.T) {
    provider := NewProvider()
    provider.Clean()

    provider.GetKnightRepository().Save(domain.Knight{ Id: "1", Name: "Joan of Arc", Strength: 10, WeaponPower: 15 })
    provider.GetKnightRepository().Save(domain.Knight{ Id: "2", Name: "Sir Daniel Fortesque", Strength: 10, WeaponPower: 15 })

    joan := provider.GetKnightRepository().Find("1")

    if (joan.Id != "1") {
        t.Fatal("Bad id returned for saved Knight using Find")
    }

    if (joan.Name != "Joan of Arc") {
        t.Fatal("Bad name returned for saved Knight using Find")
    }

    if (joan.Strength != 10) {
        t.Fatal("Bad strength returned for saved Knight using Find")
    }

    if (joan.WeaponPower != 15) {
        t.Fatal("Bad weapon power returned for saved Knight using Find")
    }
}

func TestProvider_ReadEmpty(t *testing.T) {
    provider := NewProvider()
    provider.Clean()

    provider.GetKnightRepository().Save(domain.Knight{ Id: "1", Name: "Joan of Arc", Strength: 10, WeaponPower: 15 })

    notJoan := provider.GetKnightRepository().Find("2")

    if (notJoan != nil) {
        t.Fatal("A knight as been returned while it should not")
    }
}

func TestProvider_FindAll(t *testing.T) {
    provider := NewProvider()
    provider.Clean()

    provider.GetKnightRepository().Save(domain.Knight{ Id: "1", Name: "Joan of Arc", Strength: 10, WeaponPower: 15 })
    provider.GetKnightRepository().Save(domain.Knight{ Id: "2", Name: "Sir Daniel Fortesque", Strength: 8, WeaponPower: 10 })

    knights := provider.GetKnightRepository().FindAll()

    if (knights[0].Id != "1") {
        t.Fatal("Bad id returned for saved Knight using Find")
    }

    if (knights[0].Name != "Joan of Arc") {
        t.Fatal("Bad name returned for saved Knight using Find")
    }

    if (knights[0].Strength != 10) {
        t.Fatal("Bad strength returned for saved Knight using Find")
    }

    if (knights[0].WeaponPower != 15) {
        t.Fatal("Bad weapon power returned for saved Knight using Find")
    }

    if (knights[1].Id != "2") {
        t.Fatal("Bad id returned for saved Knight using Find")
    }

    if (knights[1].Name != "Sir Daniel Fortesque") {
        t.Fatal("Bad name returned for saved Knight using Find")
    }

    if (knights[1].Strength != 8) {
        t.Fatal("Bad strength returned for saved Knight using Find")
    }

    if (knights[1].WeaponPower != 10) {
        t.Fatal("Bad weapon power returned for saved Knight using Find")
    }
}
