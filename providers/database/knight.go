package database

import (
    "log"
    "database/sql"

    _ "github.com/go-sql-driver/mysql"
    "github.com/gofrs/uuid"

    "gitlab.com/PuKoren/go-assignment/domain"
)


type knightRepository struct{
    db *sql.DB
}

func (repository *knightRepository) Find(ID string) *domain.Knight {
    knight := domain.Knight{}

	err := repository.db.
        QueryRow("SELECT id, name, strength, weapon_power FROM knight WHERE id = ?", ID).
        Scan(&knight.Id, &knight.Name, &knight.Strength, &knight.WeaponPower)

    if err == sql.ErrNoRows {
        return nil
    }

    if err != nil {
        log.Fatal(err)
    }

    return &knight
}

func (repository *knightRepository) FindAll() []*domain.Knight {
    var knights []*domain.Knight

    rows, err := repository.db.
        Query("SELECT id, name, strength, weapon_power FROM knight")

    if err != nil {
        log.Fatal(err)
    }

    defer rows.Close()

    for rows.Next() {
        knight := domain.Knight{}
        err := rows.Scan(&knight.Id, &knight.Name, &knight.Strength, &knight.WeaponPower)
        if err != nil {
            return knights
        }
        knights = append(knights, &knight)
    }

    return knights
}

func (repository *knightRepository) Clean() error {
    _, err := repository.db.Exec("TRUNCATE table knight")
    if err != nil {
        log.Println(err)
        return err
    }

    return nil
}

func (repository *knightRepository) Save(knight domain.Knight) (*domain.Knight, error) {
    if knight.Id == "" {
        knight.Id = uuid.Must(uuid.NewV4()).String()
        for repository.Find(knight.Id) != nil {
            knight.Id = uuid.Must(uuid.NewV4()).String()
        }
    }

    stmtIns, err := repository.db.Prepare("INSERT INTO knight (id, name, strength, weapon_power) VALUES (?, ?, ?, ?)")
    defer stmtIns.Close()
    _, err = stmtIns.Exec(knight.Id, knight.Name, knight.Strength, knight.WeaponPower)

    if err != nil {
        log.Println(err)
        return nil, err
    }
    
    return repository.Find(knight.Id), nil
}
