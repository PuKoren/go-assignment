package memory

import (
    "github.com/gofrs/uuid"

    "gitlab.com/PuKoren/go-assignment/domain"
)

var (
    repo []*domain.Knight
)

type knightRepository struct{}

func (repository *knightRepository) Find(ID string) *domain.Knight {
    for _, element := range repo {
        if element.GetID() == ID {
            return element
        }
    }
    return nil
}

func (repository *knightRepository) FindAll() []*domain.Knight {
    return repo
}

func (repository *knightRepository) Clean() error {
    repo = nil
    return nil
}

func (repository *knightRepository) Save(knight domain.Knight) (*domain.Knight, error) {
    if knight.Id == "" {
        knight.Id = uuid.Must(uuid.NewV4()).String()
    }

    repo = append(repo, &knight)
    return &knight, nil
}
