package domain

import (
    "errors"
)

type Knight struct {
    Id string `json:"id"`
    Strength int `json:"strength"`
    WeaponPower int `json:"weapon_power"`
    Name string `json:"name"`
}


func (knight *Knight) GetID() string {
    return knight.Id
}

func (knight *Knight) GetPower() float64 {
    return float64(knight.Strength + knight.WeaponPower)
}

func (knight *Knight) Validate() error {
    // we can't have negative or neutral strength
    if (knight.Strength <= 0) {
        return errors.New("A knight can't be this weak, it must have positive strength.")
    }

    // weapon is optional

    // we allow negative weapon power, because it can allow us
    // to use funny weapons
    // if (knight.WeaponPower < 0) {
    //     return errors.New("A weapon must have a power.")
    // }

    if (knight.Name == "") {
        return errors.New("A knight always have a name, because humans are civilized.")
    }

    return nil
}