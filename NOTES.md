# Notes

## How to test
```bash
docker-compose up -d db
# wait a few seconds for first run...
go test -p 1 ./domain ./providers/memory ./providers/database ./adapters/http
```

The database is exposed by default to host machine on port 3306 (MariaDB).
See `docker-compose.yml` file for default credentials.


## How to run
```bash

docker-compose up -d
```

Then after a few moments the service should be accessible on [the exposed port](http://localhost:8080).


# Changelog

## Modifications
  - Added Validate mehtod in Fighter interface, and used it in Knight
I needed it to validate input from the adapter. It is not ideal solution.

  - I moved error handling of "Knight not found" to the http adapter
I commented on the reasons why on the engine.fighter.go:GetKnight() method 


## Additions
  - Added route /{id1}/fight/{id2} to make two fighters fight from the API
Because this is the main feature of the project, we should expose it !

  - Added a memory provider along with the database adapter
Because it helped me to build the API step by step. I also use it in the tests of the adapter for speed reasons. The database provider have it's own integration tests.

  - Added migrations to handle SQL database properly when launching the API
I used `github.com/golang-migrate/migrate` to handle it. Migration files can be found on the `migrations` folder

  - Added a luck factor for warriors in the arena so the tables can turn for warriors having very close power

  - Added gitlab CI integration
