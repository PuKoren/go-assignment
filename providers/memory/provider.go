package memory

import "gitlab.com/PuKoren/go-assignment/engine"

type Provider struct {
    knightRepo *knightRepository
}

func (provider *Provider) GetKnightRepository() engine.KnightRepository {
    return provider.knightRepo
}

func (provider *Provider) Close() {

}

func NewProvider() *Provider {
    // todo: check table structure and apply migrations
    return &Provider{ knightRepo: &knightRepository{ } }
}

func (provider *Provider) Clean() {
    provider.knightRepo.Clean()
}